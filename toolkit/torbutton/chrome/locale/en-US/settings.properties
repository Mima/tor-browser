# Copyright (c) 2022, The Tor Project, Inc.
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

settings.categoryTitle=Connection

# Message box
settings.torPreferencesDescription=Tor Browser routes your traffic over the Tor Network, run by thousands of volunteers around the world.

# Status
settings.statusInternetLabel=Internet:
settings.statusInternetTest=Test
settings.statusInternetOnline=Online
settings.statusInternetOffline=Offline
settings.statusTorLabel=Tor Network:
settings.statusTorConnected=Connected
settings.statusTorNotConnected=Not Connected
settings.statusTorBlocked=Potentially Blocked
settings.learnMore=Learn more

# Quickstart
settings.quickstartHeading=Quickstart
settings.quickstartDescription=Quickstart connects Tor Browser to the Tor Network automatically when launched, based on your last used connection settings.
settings.quickstartCheckbox=Always connect automatically

# Bridge settings
settings.bridgesHeading=Bridges
# Old description used up to 12.0 - TODO: remove when 12.5 becomes stable:
settings.bridgesDescription=Bridges help you access the Tor Network in places where Tor is blocked. Depending on where you are, one bridge may work better than another.
settings.bridgesDescription2=Bridges help you securely access the Tor Network in places where Tor is blocked. Depending on where you are, one bridge may work better than another.
settings.bridgeLocation=Your location
settings.bridgeLocationAutomatic=Automatic
settings.bridgeLocationFrequent=Frequently selected locations
settings.bridgeLocationOther=Other locations
settings.bridgeChooseForMe=Choose a Bridge For Me…
settings.bridgeCurrent=Your Current Bridges
settings.bridgeCurrentDescription=You can keep one or more bridges saved, and Tor will choose which one to use when you connect. Tor will automatically switch to use another bridge when needed.

# Translation note: %1$S = bridge type; %2$S = bridge emoji id
settings.bridgeId=%1$S bridge: %2$S
settings.connectedBridge=Connected
settings.currentBridge=Current bridge
settings.remove=Remove
settings.bridgeDisableBuiltIn=Disable built-in bridges
settings.bridgeShare=Share this bridge using the QR code or by copying its address:
settings.whatAreThese=What are these?
settings.bridgeCopy=Copy Bridge Address
settings.copied=Copied!
settings.bridgeShowAll=Show All Bridges
settings.bridgeShowFewer=Show Fewer Bridges
settings.allBridgesEnabled=Use current bridges
settings.bridgeRemoveAll=Remove All Bridges
settings.bridgeRemoveAllDialogTitle=Remove all bridges?
settings.bridgeRemoveAllDialogDescription=If these bridges were received from torproject.org or added manually, this action cannot be undone
settings.bridgeAdd=Add a New Bridge
settings.bridgeSelectBrowserBuiltin=Choose from one of Tor Browser’s built-in bridges
settings.bridgeSelectBuiltin=Select a Built-In Bridge…
settings.bridgeRequestFromTorProject=Request a bridge from torproject.org
settings.bridgeRequest=Request a Bridge…
settings.bridgeEnterKnown=Enter a bridge address you already know
settings.bridgeAddManually=Add a Bridge Manually…

# Advanced settings
settings.advancedHeading=Advanced
settings.advancedLabel=Configure how Tor Browser connects to the internet
settings.advancedButton=Settings…
settings.showTorDaemonLogs=View the Tor logs
settings.showLogs=View Logs…

# Remove all bridges dialog
settings.removeBridgesQuestion=Remove all the bridges?
settings.removeBridgesWarning=This action cannot be undone.
settings.cancel=Cancel

# Scan bridge QR dialog
settings.scanQrTitle=Scan the QR code

# Builtin bridges dialog
settings.builtinBridgeHeader=Select a Built-In Bridge
settings.builtinBridgeDescription2=Tor Browser includes some specific types of bridges known as “pluggable transports”, which can help conceal the fact you’re using Tor.
settings.builtinBridgeObfs4Title=obfs4 (Built-in)
settings.builtinBridgeObfs4Description2=Makes your Tor traffic look like random data. May not work in heavily censored regions.
settings.builtinBridgeSnowflake=Snowflake
settings.builtinBridgeSnowflakeDescription2=Routes your connection through Snowflake proxies to make it look like you’re placing a video call, for example.
settings.builtinBridgeMeekAzure=meek-azure
settings.builtinBridgeMeekAzureDescription2=Makes it look like you’re connected to a Microsoft website, instead of using Tor. May work in heavily censored regions, but is usually very slow.
settings.bridgeButtonConnect=Connect
settings.bridgeButtonAccept=OK

# Old dialog strings used up to 12.0 - TODO: remove when 12.5 becomes stable:
settings.builtinBridgeTitle=Built-In Bridges
settings.builtinBridgeDescription=Tor Browser includes some specific types of bridges known as “pluggable transports”.
settings.builtinBridgeObfs4=obfs4
settings.builtinBridgeObfs4Description=obfs4 is a type of built-in bridge that makes your Tor traffic look random. They are also less likely to be blocked than their predecessors, obfs3 bridges.
settings.builtinBridgeSnowflakeDescription=Snowflake is a built-in bridge that defeats censorship by routing your connection through Snowflake proxies, ran by volunteers.
settings.builtinBridgeMeekAzureDescription=meek-azure is a built-in bridge that makes it look like you are using a Microsoft web site instead of using Tor.
# end

# Request bridges dialog
settings.requestBridgeDialogTitle=Request Bridge
settings.submitCaptcha=Submit
settings.contactingBridgeDB=Contacting BridgeDB. Please Wait.
settings.solveTheCaptcha=Solve the CAPTCHA to request a bridge.
settings.captchaTextboxPlaceholder=Enter the characters from the image
settings.incorrectCaptcha=The solution is not correct. Please try again.

# Provide bridge dialog
settings.provideBridgeTitleAdd=Add a Bridge Manually
# Translation note: %S is a Learn more link.
settings.provideBridgeDescription=Add a bridge provided by a trusted organization or someone you know. If you don’t have a bridge, you can request one from the Tor Project. %S
settings.provideBridgePlaceholder=type address:port (one per line)

# Connection settings dialog
settings.connectionSettingsDialogTitle=Connection Settings
settings.connectionSettingsDialogHeader=Configure how Tor Browser connects to the Internet
settings.useLocalProxy=I use a proxy to connect to the Internet
settings.proxyType=Proxy Type
settings.proxyTypeSOCKS4=SOCKS4
settings.proxyTypeSOCKS5=SOCKS5
settings.proxyTypeHTTP=HTTP/HTTPS
settings.proxyAddress=Address
settings.proxyAddressPlaceholder=IP address or hostname
settings.proxyPort=Port
settings.proxyUsername=Username
settings.proxyPassword=Password
settings.proxyUsernamePasswordPlaceholder=Optional
settings.useFirewall=This computer goes through a firewall that only allows connections to certain ports
settings.allowedPorts=Allowed Ports
settings.allowedPortsPlaceholder=Comma-seperated values

# Log dialog
settings.torLogDialogTitle=Tor Logs
settings.copyLog=Copy Tor Log to Clipboard

# Legacy strings - remove once 12.0 has gone EOL
settings.provideBridgeTitle=Provide Bridge
settings.provideBridgeHeader=Enter bridge information from a trusted source
